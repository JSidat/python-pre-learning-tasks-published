def vowel_swapper(string):
    # ==============
    # Your code here
    swap = {"A": "4", "a": "4", "E": "3", "e": "3", "I": "!",
            "i": "!", "O": "000", "o": "ooo", "U": "|_|", "u": "|_|"}
    occurrences = {"a": 0, "e": 0, "i": 0, "o": 0, "u": 0} # initialise dictionary with letter count

    string = list(string) # split the string into a list of elements
    for index, i in enumerate(string): # index stores the position of each element in the list as you iterate through the list
        if string[index] in swap.keys(): # string[index] gives each letter of list & compares to keys in swap dictionary
            occurrences[string[index].lower()] += 1 # if letter present i.e. a vowel, increment the count of letter by 1
            if occurrences[string[index].lower()] == 2: # if the count of the letter is 2,  
                string[index] = swap[i] # swap the value of the letter in the list with the value of the corresponding key in swap
    return ''.join(string) # convert the list back to string 


        
# Expected results for this problem are different on gitlab compared to the ones below. My solutions match the ones on gitlab 
            
           
           
    
    
    # ==============
print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console

