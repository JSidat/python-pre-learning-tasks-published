from collections import deque

def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        number = a+b
    elif operator == "-":
        number = a-b
    elif operator == "*":
        number = a*b
    else:
        number = int(a/b)
    
    d = deque() # initiate an empty list
    while number >= 1: # keep dividing the number by 2 until it reaches 1 (quotient becomes 0 when 1 divided by 2)
        d.appendleft(str(number % 2)) # add the remainder of each division to the list. str used to allow for join() to work later
        number //= 2 # increment the number by dividing by 2 (while loop will run until this increment reaches 1)
    return ''.join(d) # join the elements in the list and return the string in reverse to give the correct binary value
    
    
        

    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console


