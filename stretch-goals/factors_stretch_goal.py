def factors(number):
    # ==============
    # Your code here
    factors = []
    for i in range(2, (number//2)+1):
        if number % i == 0:
            factors.append(i)
    if len(factors) > 0: # if the factors list has elements, the number is not prime as we have excluded 1 & number itself  
        return factors # therefore return te factors list
    return str(number) + " is a prime number" # if no elements present, there are no factors, the number is a prime number

    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
