def vowel_swapper(string):
    # ==============
    # Your code here
    swap = {"A": "4", "a": "4", "E": "3", "e": "3", "I": "!",
            "i": "!", "O": "000", "o": "ooo", "U": "|_|", "u": "|_|"}

    string = list(string)

    for index, i in enumerate(string):
        if i in swap.keys():
            string[index] = swap[i]
    return ''.join(string)

        

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
